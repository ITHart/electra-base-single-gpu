# Installation

## Basics
    python3 -m venv venv
    source venv/bin/activate
    python3 -m pip install torch cython

## Deepspeed
Requires a specific GPU (e.g. NVIDIA V100)

    python -m pip install deepspeed mpi4py

### Compile code
    sh compile_cython.sh

## Transformers
Tested with 4.6.1. Feel free to try a newer version, but the code might need to be tweaked

    git clone -b v4.6.1 https://github.com/huggingface/transformers.git
    cd transformers
    python3 -m pip install -e .

### GLUE requirements
    python3 -m pip install -r ./examples/pytorch/_tests_requirements.txt


# Usage

## Prepare data

### Create and shuffle dataset
This requires ~66GB of space and ~40GB or RAM.
(token_files folder can be removed afterwards to save space.)

    python3 create_owt_token_files.py config/vocab.txt data/tokens/token_files 512 64000
    python3 shuffle_owt_token_files.py data/tokens/token_files _shuffle 512 64000

### Create TNF mask files
This requires ~33B of space.
Calculate training step count before hand. TNF files have to match it.
In this case: 50000 steps * 8 gradient accumulation * 24 batch size = 9600000

    python3 create_TNF_word_list.py data/tokens/token_files_shuffle config/vocab.txt data/tnf/words_50k 9600000 100 500

    python3 create_TNF_mask_files.py data/tokens/token_files_shuffle config/vocab.txt data/tnf/words_50k data/tnf/masks_50k

## Pretrain model

### No optimizations
    sh pretrain.sh data token_files_shuffle electra-model 24 10000 8 1e-04 1 0 0 0 0 123456789

### Progressive stacking
    sh pretrain.sh data token_files_shuffle electra-model 24 10000 8 1e-04 1 1 111 0 0 123456789

### Taking Notes on the Fly
    sh pretrain.sh data token_files_shuffle electra-model 24 10000 8 1e-04 1 0 0 1 5e-02 123456789

### With deepspeed
Add path to a deepspeed config file as the last argument, e.g config/ds_config_z1.
Can boost batch size up to ~42.

## Test model
    sh run_glue.sh data/models/electra-model cola 1 tmp glue.out

Task can be one of:  
cola, sst2, mrpc, rte, wnli, qnli, stsb, mnli, qqp

The script appends to the output file instead of overwriting it.

# License

See included MIT license file.
