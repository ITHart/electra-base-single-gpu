import sys, os
from token_files_dataset import TokenFilesDataset
import fast_functions
import array

import logging
logger = logging.getLogger(__name__)

logging.basicConfig(
	format="%(asctime)s - %(message)s",
	datefmt="%m/%d/%Y %H:%M:%S",
	level=logging.INFO,
)

# Read parameters
dataset_dir = sys.argv[1]
vocab_path = sys.argv[2]
rare_words_path = sys.argv[3]
output_dir = sys.argv[4]

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Initialize data
ds = TokenFilesDataset(dataset_dir)
vocab, _, is_hash_token = fast_functions.create_vocab_tables(vocab_path) 
rare_word_to_id, _ = fast_functions.create_rare_word_tables(rare_words_path)

tokens_per_line = 512
step = 0
file_index = 0
f = None
maxLen = len(ds)

# Create mask files using the token files structure
for data in ds:
	mask_line = fast_functions.get_rare_words_mask(data.detach().cpu().numpy(), tokens_per_line, vocab, is_hash_token, rare_word_to_id)
	
	# Open new file
	if f == None:
		file_index = ds.file_index
		f = open(os.path.join(output_dir, str(file_index - 1)), "wb")

	# Write line to file
	byte_array = array.array("i", mask_line.tolist())
	f.write(byte_array)

	# File is full of lines. Close it!
	if file_index != ds.file_index:
		f.close()
		f = None

	step += 1
	if step % 100000 == 0:
		logger.info(f"{step} / {maxLen}")
	# Debug
	#if step == 2:
	#	f.close()
	#	break
		
logger.info(f"Wrote {file_index} files to {output_dir}")

# Debug
#with open(os.path.join(output_dir, "0"), "rb") as f:
#	a = array.array("i")
#	for i in range(3):
#		a.fromfile(f, 512)
#		print(i)
#		print(a)
