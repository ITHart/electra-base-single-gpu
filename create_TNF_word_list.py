import sys, os
from token_files_dataset import TokenFilesDataset
from fast_functions import create_vocab_tables
import fast_functions

import logging
logger = logging.getLogger(__name__)

logging.basicConfig(
	format="%(asctime)s - %(message)s",
	datefmt="%m/%d/%Y %H:%M:%S",
	level=logging.INFO,
)

# Read parameters
dataset_path = sys.argv[1]
vocab_path = sys.argv[2]
output_path = sys.argv[3]
max_steps = int(sys.argv[4])
min_freq = int(sys.argv[5])
max_freq = int(sys.argv[6])

output_dir = os.path.dirname(output_path)
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

logger.info(f"Output: {output_path}, min: {min_freq}, max: {max_freq}")

# Initialize datasets
ds = TokenFilesDataset(dataset_path)
vocab, inv_vocab, is_hash_word = create_vocab_tables(vocab_path)

# Count frequencies
freq = {}
step = 0

max_len = len(ds)
if max_steps > 0:
	max_len = min(max_steps, max_len)

for data in ds:
	words = fast_functions.get_words(data.detach().cpu().numpy(), 512, vocab, is_hash_word)

	for word in words:
		if not word in freq:
			freq[word] = 0
		freq[word] += 1

	step += 1
	if step % 100000 == 0:
		logger.info(f"{step} / {max_len}")
	if step == max_steps:
		break

freq = sorted(freq.items(), key=lambda x: x[1])

# Write words which are between requested frequency range
count = 0
with open(output_path, "w") as f:
	for key, value in freq:
		if min_freq <= value and value <= max_freq:
			count += 1
			f.write(key + "\n")
			logger.info(f"{key} : {value}")
		
logger.info(f"Wrote {count} lines to {output_path}")
