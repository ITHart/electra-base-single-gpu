# Creates binary token files from openwebtext dataset
import sys, os, time, array

from datasets import load_dataset
from electra_tokenizer import ElectraTokenizer
from fast_functions import create_vocab_tables

import logging
logger = logging.getLogger(__name__)
logging.basicConfig(format="%(message)s", level=logging.INFO)

# Read params
vocab_dir = sys.argv[1]
output_dir = sys.argv[2]
tokens_per_line = int(sys.argv[3])
lines_per_file = int(sys.argv[4])

if not os.path.exists(output_dir):
	os.makedirs(output_dir)

# Read data
dataset = load_dataset("openwebtext")
tokenizer = ElectraTokenizer(vocab_dir)
vocab, _, is_hash_token = create_vocab_tables(vocab_dir)
data = dataset["train"]

# Variable setup
max_steps = len(data)
print_steps = max_steps // 100
if print_steps == 0:
	print_steps = max_steps

f = None
file_index = 0
line_index = 0
token_index = 0

line = array.array("i", [0] * (tokens_per_line))
line[0] = tokenizer.cls_token_id

sep_token = tokenizer.sep_token_id
pad_token = tokenizer.pad_token_id

print(f"Dataset size {len(data)}")

# Iterate dataset and create token files
start_time = time.time()

for sid in range(max_steps):
	# Convert words to tokens
	tokens = tokenizer.encode_plus(data[sid]["text"], add_special_tokens=False, return_attention_mask=False,return_token_type_ids=False)["input_ids"]

	while len(tokens) > 0:
		# Calculate how many tokens can be added to line
		add_count = 0
		if token_index + len(tokens) < tokens_per_line - 1:
			# All of the tokens fit
			add_count = len(tokens)
		else:
			# Only some of the tokens fit
			add_count = tokens_per_line - 2 - token_index

			# Make sure that last word is not cut in half
			while(is_hash_token[tokens[add_count]]):
				add_count -= 1

		# Insert tokens to line
		added_count = 0
		while(added_count < add_count):
			token_index += 1
			line[token_index] = tokens[added_count]
			added_count += 1

		# Cut added tokens from tokens
		tokens = tokens[added_count:]

		# Add sep token between sentences
		if added_count > 0:
			token_index += 1
			line[token_index] = sep_token
	
		# Line is full -> write line to file
		if token_index == tokens_per_line - 2 or added_count == 0:
			# Add pad tokens if line was not filled 
			for t in range(token_index + 1, tokens_per_line):
				line[t] = pad_token

			# Open new file
			if f == None:
				line_index = 0
				f = open(os.path.join(output_dir, str(file_index)), "wb")
				file_index += 1

			f.write(line)

			# Debug. Print line to see that everything works
			#for t in line:
			#	if is_hash_token[t] == 0:
			#		print(" ", end="")
			#	print(vocab[t], end="")
			#print()
			
			# Go to beginning of next line
			token_index = 0
			line_index += 1

			# File is full of lines -> close it!
			if line_index == lines_per_file:
				f.close()
				f = None
	
	# Progress counter
	if sid % print_steps == 0:
		logger.info(f"{sid}/{max_steps}")
	# Debug
	#if sid == 2000:
	#	f.close()
	#	break

# Close lasts dangling file. It does not have full line count, but each line is full of tokens 
# That is, last imcomplete line is dropped
if f != None:
	f.close()

print("Time:", time.time() - start_time)
print(f"Wrote {file_index} files to {output_dir}")
