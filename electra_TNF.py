# coding=utf-8
# Copyright 2019 The Google AI Language Team Authors and The HuggingFace Inc. team.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Custom pretraining PyTorch ELECTRA model with support for Taking Notes on the Fly optimization."""

import torch
import torch.nn as nn

from transformers.models.electra.modeling_electra import (
	ElectraModel,
	ElectraGeneratorPredictions,
	ElectraEmbeddings,
	ElectraPreTrainedModel,
	ElectraDiscriminatorPredictions,
	ElectraForPreTrainingOutput
)

from transformers.modeling_outputs import MaskedLMOutput
from transformers.models.bert.modeling_bert import BertEncoder

from transformers.utils import logging
logger = logging.get_logger(__name__)

class ElectraModelWithTNF(ElectraPreTrainedModel):
	def __init__(self, config, training_args, sequence_length):
		super().__init__(config)
		self.embeddings = ElectraEmbeddings(config)
		
		if config.embedding_size != config.hidden_size:
			self.embeddings_project = nn.Linear(config.embedding_size, config.hidden_size)

		self.encoder = BertEncoder(config)
		self.config = config

		# TNF setup
		self.use_tnf = training_args.use_TNF
		if self.use_tnf:
			self.tnf_notes = nn.Embedding(training_args.TNF_word_count, config.embedding_size)
			self.tnf_notes.weight.requires_grad=False
			self.tnf_notes.weight.data.normal_(mean=0.0, std=config.initializer_range)

			self.tnf_note_weight = training_args.TNF_note_weight
			self.tnf_update_factor = training_args.TNF_update_factor
			self.tnf_window = training_args.TNF_window
			self.tnf_half_window = self.tnf_window // 2

		self.init_weights()

	def get_input_embeddings(self):
		return self.embeddings.word_embeddings

	def set_input_embeddings(self, value):
		self.embeddings.word_embeddings = value

	def _prune_heads(self, heads_to_prune):
		"""Prunes heads of the model.
		heads_to_prune: dict of {layer_num: list of heads to prune in this layer}
		See base class PreTrainedModel
		"""
		for layer, heads in heads_to_prune.items():
			self.encoder.layer[layer].attention.prune_heads(heads)

	def forward(
		self,
		input_ids=None,
		attention_mask=None,
		token_type_ids=None,
		position_ids=None,
		head_mask=None,
		inputs_embeds=None,
		output_attentions=None,
		output_hidden_states=None,
		return_dict=None,
		tnf_masks=None
	):
		output_attentions = output_attentions if output_attentions is not None else self.config.output_attentions
		output_hidden_states = (
			output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states
		)
		return_dict = return_dict if return_dict is not None else self.config.use_return_dict

		if input_ids is not None and inputs_embeds is not None:
			raise ValueError("You cannot specify both input_ids and inputs_embeds at the same time")
		elif input_ids is not None:
			input_shape = input_ids.size()
		elif inputs_embeds is not None:
			input_shape = inputs_embeds.size()[:-1]
		else:
			raise ValueError("You have to specify either input_ids or inputs_embeds")

		device = input_ids.device if input_ids is not None else inputs_embeds.device

		if attention_mask is None:
			attention_mask = torch.ones(input_shape, device=device)
		if token_type_ids is None:
			token_type_ids = torch.zeros(input_shape, dtype=torch.long, device=device)

		extended_attention_mask = self.get_extended_attention_mask(attention_mask, input_shape, device)
		head_mask = self.get_head_mask(head_mask, self.config.num_hidden_layers)

		hidden_states = self.embeddings(
			input_ids=input_ids, position_ids=position_ids, token_type_ids=token_type_ids, inputs_embeds=inputs_embeds
		)

		if hasattr(self, "embeddings_project"):
			hidden_states = self.embeddings_project(hidden_states)
		
		# TNF get embeddings for rare words
		if self.use_tnf:
			# Mix note embeddings with input and position embeddings
			mask_positions = tnf_masks != -1
			hidden_states[mask_positions] = hidden_states[mask_positions] * (1.0 - self.tnf_note_weight) + \
				self.tnf_notes.weight.data[tnf_masks.masked_select(mask_positions)] * self.tnf_note_weight

		hidden_states = self.encoder(
			hidden_states,
			attention_mask=extended_attention_mask,
			head_mask=head_mask,
			output_attentions=output_attentions,
			output_hidden_states=output_hidden_states,
			return_dict=return_dict,
		)

		return hidden_states

	def update_TNF_notes(self, x, tnf_masks):	
		if self.use_tnf:
			mask_positions = tnf_masks != -1
			# TNF source code includes two ways of calculating notes. Moving window averaging is explained in the paper
			# so it was used here. This code is adapted directly from the source code and simplified slightly.
			
			# Calculate moving window averages for each token in encoder output.
			# This version only works properly if sequence length is evenly divisible by window size.
			# (defaults 512 and 32 are fine.)
			x_cumsum = torch.cumsum(x, dim=1)
			x_sum_left = x_cumsum[:, self.tnf_window-1, :].unsqueeze(dim=1).expand(-1, self.tnf_half_window, -1)
			x_sum_middle = x_cumsum[:, self.tnf_window:, :] - x_cumsum[:, :-self.tnf_window, :]
			x_sum_right = x_sum_middle[:, -1, :].unsqueeze(dim=1).expand(-1, self.tnf_half_window, -1)
			x_averages = torch.cat([x_sum_left, x_sum_middle, x_sum_right], dim=1) / self.tnf_window
		
			# Update notes based on average values.
			sorted_mask, sorted_index = torch.sort(tnf_masks[mask_positions].view(-1))
			mask_averages = x_averages[mask_positions, :].contiguous().view(-1, x.size(-1))[sorted_index]
			word_indices, word_lengths = torch.unique_consecutive(sorted_mask, return_counts=True)
			cum_averages = torch.cumsum(mask_averages.float(), dim=0)
			word_end_indices = torch.cumsum(word_lengths, dim=0) - 1
			a = torch.index_select(cum_averages, 0, word_end_indices)
			b = torch.cat([torch.zeros((1, x.size(-1)), dtype=a.dtype, device=a.device), a[:-1, :]], dim=0)
			notes_update = (a - b).type_as(x)

			self.tnf_notes.weight.data[word_indices] = self.tnf_notes.weight.data[word_indices] * (1.0 - self.tnf_update_factor) + notes_update * self.tnf_update_factor

class ElectraForPreTrainingWithTNF(ElectraPreTrainedModel):
	def __init__(self, config, training_args, sequence_length):
		super().__init__(config)

		self.electra = ElectraModelWithTNF(config, training_args, sequence_length)
		self.discriminator_predictions = ElectraDiscriminatorPredictions(config)
		self.init_weights()

	def forward(
		self,
		input_ids=None,
		attention_mask=None,
		token_type_ids=None,
		position_ids=None,
		head_mask=None,
		inputs_embeds=None,
		labels=None,
		output_attentions=None,
		output_hidden_states=None,
		return_dict=None,
		tnf_masks = None
	):	
		return_dict = return_dict if return_dict is not None else self.config.use_return_dict

		discriminator_hidden_states = self.electra(
			input_ids,
			attention_mask,
			token_type_ids,
			position_ids,
			head_mask,
			inputs_embeds,
			output_attentions,
			output_hidden_states,
			return_dict,
			tnf_masks,
		)
		discriminator_sequence_output = discriminator_hidden_states[0]

		logits = self.discriminator_predictions(discriminator_sequence_output)

		loss = None
		if labels is not None:
			loss_fct = nn.BCEWithLogitsLoss()
			if attention_mask is not None:
				active_loss = attention_mask.view(-1, discriminator_sequence_output.shape[1]) == 1
				active_logits = logits.view(-1, discriminator_sequence_output.shape[1])[active_loss]
				active_labels = labels[active_loss]
				loss = loss_fct(active_logits, active_labels.float())
			else:
				loss = loss_fct(logits.view(-1, discriminator_sequence_output.shape[1]), labels.float())

		if not return_dict:
			output = (logits,) + discriminator_hidden_states[1:]
			return ((loss,) + output) if loss is not None else output

		return ElectraForPreTrainingOutput(
			loss=loss,
			logits=logits,
			hidden_states=discriminator_hidden_states.hidden_states,
			attentions=discriminator_hidden_states.attentions,
		)

# Same as original, but returns necessary prediction output
class ElectraForMaskedLM(ElectraPreTrainedModel):
	def __init__(self, config):
		super().__init__(config)

		self.electra = ElectraModel(config)
		self.generator_predictions = ElectraGeneratorPredictions(config)

		self.generator_lm_head = nn.Linear(config.embedding_size, config.vocab_size)
		self.init_weights()

	def get_output_embeddings(self):
		return self.generator_lm_head

	def set_output_embeddings(self, word_embeddings):
		self.generator_lm_head = word_embeddings

	def forward(
		self,
		input_ids=None,
		attention_mask=None,
		token_type_ids=None,
		position_ids=None,
		head_mask=None,
		inputs_embeds=None,
		labels=None,
		output_attentions=None,
		output_hidden_states=None,
		return_dict=None,
	):
		r"""
		labels (:obj:`torch.LongTensor` of shape :obj:`(batch_size, sequence_length)`, `optional`):
			Labels for computing the masked language modeling loss. Indices should be in ``[-100, 0, ...,
			config.vocab_size]`` (see ``input_ids`` docstring) Tokens with indices set to ``-100`` are ignored
			(masked), the loss is only computed for the tokens with labels in ``[0, ..., config.vocab_size]``
		"""
		return_dict = return_dict if return_dict is not None else self.config.use_return_dict
		
		generator_hidden_states = self.electra(
			input_ids,
			attention_mask,
			token_type_ids,
			position_ids,
			head_mask,
			inputs_embeds,
			output_attentions,
			output_hidden_states,
			return_dict,
		)
		generator_sequence_output = generator_hidden_states[0]
		prediction_out = self.generator_predictions(generator_sequence_output)
		prediction_scores = self.generator_lm_head(prediction_out)

		loss = None
		# Masked language modeling softmax layer
		if labels is not None:
			loss_fct = nn.CrossEntropyLoss()  # -100 index = padding token
			loss = loss_fct(prediction_scores.view(-1, self.config.vocab_size), labels.view(-1))

		if not return_dict:
			# Returns prediction output as well as scores
			output = (prediction_scores, prediction_out) + generator_hidden_states[1:]
			return ((loss,) + output) if loss is not None else output

		return MaskedLMOutput(
			loss=loss,
			logits=prediction_scores,
			hidden_states=generator_hidden_states.hidden_states,
			attentions=generator_hidden_states.attentions,
		)
