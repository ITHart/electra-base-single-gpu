from transformers import PreTrainedTokenizer
import torch
from dataclasses import dataclass

@dataclass
class ElectraCollator:
	tokenizer: PreTrainedTokenizer
	use_TNF: bool

	def __call__(self, examples):
		masks = None

		if self.use_TNF:
			tokens = [e[0] for e in examples]
			masks = [e[1] for e in examples]

			masks = torch.stack(masks, dim=0)
		else:
			tokens = examples
			
		tokens = torch.stack(tokens, dim=0)

		labels = tokens.clone().detach()
		if self.tokenizer.pad_token_id is not None:
			labels[labels == self.tokenizer.pad_token_id] = -100

		return {"input_ids": tokens, "labels": labels, "tnf_masks": masks}
		
