from transformers import BertTokenizerFast

class ElectraTokenizer(BertTokenizerFast):
    def __init__(self, vocab_file, **kwargs):
        super().__init__(
            vocab_file=vocab_file,
			cls_token="[CLS]",
			sep_token="[SEP]",
			pad_token="[PAD]",
            unk_token="[UNK]",
            mask_token="[MASK]",
            **kwargs,
        )
