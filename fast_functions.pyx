#  cython: language_level=3, boundscheck=False, wraparound=False, cdivision=True, initializedcheck=False
import numpy as np

def create_vocab_tables(vocab_dir):
	vocab = []	
	is_hash_token = []
	with open(vocab_dir) as f:
		for line in f:
			token = line.strip()
			if token.startswith("##"):
				vocab.append(token[2:])
				is_hash_token.append(1)
			else:
				vocab.append(token)
				is_hash_token.append(0)
      
	inv_vocab = {token: index for index, token in enumerate(vocab)}
	return vocab, inv_vocab, np.array(is_hash_token, dtype=np.int32)

def create_rare_word_tables(rare_words_file_path):
	rare_word_to_id = {}
	rare_words = []
	with open(rare_words_file_path) as f:
		for line in f:
			word = line.strip()
			rare_word_to_id[word] = len(rare_words)
			rare_words.append(word)

	return rare_word_to_id, rare_words

# Fast'ish helper functions for data scripts (unicode string concat is not fast in Cython unfortunately)
def get_words(long[:] indices, int sequence_size, vocab, int[::1] is_hash_token):
	words = []
	cdef int i, token_id
	word = ""

	# Skip CLS, SEP and PAD
	for i in range(sequence_size -2, 0, -1):
		token_id = indices[i]
		
		# Magic numbers alert!
		if token_id == 0 or token_id == 102:
			continue

		word = vocab[token_id] + word

		if is_hash_token[token_id] == 0:
			words.append(word)
			word = ""

	return words

# Returns an array with rare word indices. Common word indices are set to -1.
def get_rare_words_mask(long [:] indices, int sequence_size, vocab, int[::1] is_hash_token, rare_words_to_id):
	mask_np = np.zeros(sequence_size, dtype=np.int32)
	cdef int[:] mask = mask_np
	cdef int i, j, word_length = 0, word_id, token_id
	word = ""

	for i in range(sequence_size -1, -1, -1):
		token_id = indices[i]
		word = vocab[token_id] + word
		word_length += 1

		if is_hash_token[token_id] == 0:
			word_id = -1
			if word in rare_words_to_id:
				word_id = rare_words_to_id[word]
		
			for j in range(word_length):
				mask[i + j] = word_id
			word_length = 0
			word = ""

	return mask_np

