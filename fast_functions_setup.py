from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
	name = "fast_functions",
	cmdclass = {"build_ext": build_ext},
	ext_modules =
	[Extension("fast_functions",
		["fast_functions.pyx"],
		extra_compile_args = ["-O2"]
		)]
)

