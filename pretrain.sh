#!/bin/bash
DATA_DIR=$1
DATA_SET=$2
MODEL_NAME=$3
BATCH_SIZE=$4
TRAIN_STEPS=$5
ACCUMULATOR=$6
LEARNING_RATE=$7
USE_CONSTANT=$8
USE_PS=$9
PS_MASK=${10}
USE_TNF=${11}
TNF_WEIGHT=${12}
SEED=${13}

SAVE_COUNT=2
SAVE_STEPS=$((TRAIN_STEPS / SAVE_COUNT))

if [[ $# -ge 14 ]] ; then
	echo Using deepspeed
	DS_CONFIG=${14}
	# Checkpoints don't work with DS for some reason
	SAVE_STEPS=999999999
fi

WARMUP_STEPS=$((TRAIN_STEPS / 100))
STEPS_K=$((TRAIN_STEPS / 1000))k
echo $WARMUP_STEPS $STEPS_K

mkdir -p out

python3 -u run_pretraining.py \
	--per_device_train_batch_size $BATCH_SIZE \
	--dataset_dir $DATA_DIR/tokens/$DATA_SET \
	--output_dir $DATA_DIR/models/$MODEL_NAME \
	--discriminator_config_name config/discriminator.json \
	--generator_config_name config/generator.json \
	--tokenizer_name config \
	--cache_dir $DATA_DIR/cache \
	--do_train \
	--use_TNF $USE_TNF \
	--TNF_note_weight $TNF_WEIGHT \
	--TNF_word_list_path $DATA_DIR/tnf/words_$STEPS_K \
	--TNF_mask_files_path $DATA_DIR/tnf/masks_$STEPS_K \
	--use_PS $USE_PS --PS_k 4 \
	--PS_mask $PS_MASK \
	--learning_rate $LEARNING_RATE \
	--use_constant_scheduler $USE_CONSTANT \
	--adam_epsilon 1e-6 \
	--num_train_epochs 1 \
	--max_steps $TRAIN_STEPS \
	--max_eval_steps -1 --eval_steps -1 \
	--save_total_limit $SAVE_COUNT --save_steps $SAVE_STEPS \
	--warmup_steps $WARMUP_STEPS --logging_steps 20 \
	--gradient_accumulation_steps $ACCUMULATOR \
	--seed $SEED --block_size 512 \
	--deepspeed_config $DS_CONFIG \
	--fp16 --fp16_opt_level O1 \
	--report_to none \
	--overwrite_output_dir > out/$MODEL_NAME.out
