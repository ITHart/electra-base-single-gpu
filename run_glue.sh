#!/bin/bash
MODEL_PATH=$1
TASK_NAME=$2
TRAIN=$3
TMP_DIR=$4
OUT_FILE=$5
OUT_DIR=out
mkdir -p $OUT_DIR

echo Running $TASK_NAME benchmark

python3 -u transformers/examples/pytorch/text-classification/run_glue.py \
    --model_name_or_path $MODEL_PATH \
    --task_name $TASK_NAME \
    --do_train $TRAIN \
    --do_eval \
    --cache_dir $TMP_DIR \
    --max_seq_length 128 \
    --per_device_eval_batch_size=32   \
    --per_device_train_batch_size=32   \
	--save_steps 999999999 \
    --learning_rate 2e-5 \
    --num_train_epochs 3.0 \
    --overwrite_output_dir \
	--fp16 \
	--report_to none \
    --output_dir $TMP_DIR/$TASK_NAME >> $OUT_DIR/$OUT_FILE
