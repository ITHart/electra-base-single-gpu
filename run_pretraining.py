# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors and The HuggingFace Inc. team.
# Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Pre-training language models using the ELECTRA method.
"""

import math
import multiprocessing
import os, shutil
import tarfile
from dataclasses import dataclass, field
from itertools import chain
from pathlib import Path
from typing import Dict, Optional, Tuple, Union

import numpy as np
import torch
import torch.nn as nn

from transformers import (
	AutoConfig,
	AutoTokenizer,
	HfArgumentParser,	
	Trainer,
	set_seed, 
	PretrainedConfig
)

from transformers.modeling_utils import PreTrainedModel
from transformers.training_args import TrainingArguments
from electra_tokenizer import ElectraTokenizer

from transformers.optimization import AdamW, get_linear_schedule_with_warmup, get_constant_schedule_with_warmup

from fast_functions import create_rare_word_tables
from electra_collator import ElectraCollator
from electra_TNF import ElectraForPreTrainingWithTNF, ElectraForMaskedLM
from token_files_dataset import TokenFilesDataset

import logging
logger = logging.getLogger(__name__)

import time, datetime

@dataclass
class ModelArguments:
	"""
	Arguments pertaining to which model/config/tokenizer we are going to fine-tune, or train from scratch.
	"""

	discriminator_name_or_path: Optional[str] = field(
		default=None,
		metadata={
			"help": "The discriminator checkpoint for weights initialization. Leave None if you want to train a model "
			"from scratch."
		},
	)
	discriminator_config_name: Optional[str] = field(
		default=None,
		metadata={"help": "Pretrained config name or path if not the same as the discriminator model_name"},
	)
	generator_name_or_path: Optional[str] = field(
		default=None,
		metadata={
			"help": "The generator checkpoint for weights initialization. Leave None if you want to train a model "
			"from scratch."
		},
	)
	generator_config_name: Optional[str] = field(
		default=None, metadata={"help": "Pretrained config name or path if not the same as the generator model_name"}
	)
	tokenizer_name: Optional[str] = field(
		default=None,
		metadata={"help": "Pretrained tokenizer name or path if not the same as discriminator model_name"},
	)
	cache_dir: Optional[str] = field(
		default=None, metadata={"help": "Where do you want to store the pretrained models downloaded from s3"}
	)

@dataclass
class DataTrainingArguments:
	"""
	Arguments pertaining to what data we are going to input our model for training and eval.
	"""
	train_data_file: Optional[str] = field(
		default=None, metadata={"help": "The input training data file (a text file)."}
	)
	eval_data_file: Optional[str] = field(
		default=None,
		metadata={"help": "An optional input evaluation data file to evaluate the perplexity on (a text file)."},
	)
	dataset_dir: Optional[str] = field(
		default=None,
		metadata={"help": "The directory containing files that will be used for training and evaluation."},
	)
	block_size: int = field(
		default=-1,
		metadata={
		"help": "Optional input sequence length after tokenization."
		"The training dataset will be truncated in block of this size for training."
		"Default to the model max input length for single sentence inputs (take into account special tokens)."
		},
	)
	overwrite_cache: bool = field(
		default=False, metadata={"help": "Overwrite the cached training and evaluation sets"}
	)
	num_dataset_building_processes: int = field(
		default=1, metadata={"help": "The number of workers that will be used to build the dataset."}
	)
	num_tensors_per_file: int = field(
		default=2048,
		metadata={
		"help": "The number of tensors that will be stored in each file after tokenization."
		"The smaller the amount, the smaller the filesize, but the larger the amount"
		"of files that will be created."
		},
	)
	mask_probability: float = field(
		default=0.15, metadata={"help": "Percentage of the input that will be masked or replaced."}
	)
	max_predictions_per_sequence: int = field(
		default=-1, metadata={"help": "Maximum tokens that will be masked in a sequence."},
	)
	tmp_dir: Optional[str] = field(default=None)

@dataclass
class ElectraTrainingArguments(TrainingArguments):
	max_steps: int = field(
		default=1_000_000,
		metadata={"help": "If > 0: set total number of training steps to perform. Override num_train_epochs."},
	)
	max_eval_steps: int = field(
		default=100, metadata={"help": "If > 0: set total number of eval steps to perform."},
	)
	warmup_steps: int = field(default=10_000, metadata={"help": "Linear warmup over warmup_steps."})
	weight_decay: float = field(default=0.1, metadata={"help": "Weight decay if we apply some."})
	generator_weight: float = field(default=1.0, metadata={"help": "Weight coefficient for the generator loss"})
	discriminator_weight: float = field(
		default=50.0, metadata={"help": "Weight coefficient for the discriminator loss"}
	)
	use_constant_scheduler: bool = field(default = False)

	deepspeed_config: str = field(default=None) # For optional sh script input
 
    # Taking Notes on the Fly
	use_TNF: bool = field(default=False)
	TNF_word_list_path: str = field(default=None)
	TNF_mask_files_path: str = field(default=None)
	TNF_weight: float = field(default=0.5)
	TNF_note_weight: float = field(default=0.5)
	TNF_update_factor: float = field(default=0.1)
	TNF_window: float = field(default=32)
	TNF_word_count: int = field(default=0) # Hack. Actually set in code
 
	# Progressive stacking
	use_PS: int = field(default=0)
	use_PS: bool = field(default=False)
	PS_k: int = field(default=4)
	PS_mask: str = field(default="0b111") # String due to sh script input

	def __post_init__(self):
		if self.deepspeed_config:
			self.deepspeed=self.deepspeed_config
		super().__post_init__()


def get_dataset(data_args: DataTrainingArguments, training_args):
	TNF_mask_files_path = None
	if training_args.use_TNF:
		TNF_mask_files_path = training_args.TNF_mask_files_path

	return TokenFilesDataset(data_args.dataset_dir, TNF_mask_files_path, data_args.block_size)

class CombinedModel(PreTrainedModel):
	def __init__(self,
				discriminator_config,
				generator_config,
				tokenizer,
				training_args: ElectraTrainingArguments,
				data_args: DataTrainingArguments,
				model_args
				):
		#super().__init__(PretrainedConfig())
		super().__init__(discriminator_config)

		self.sequence_length = data_args.block_size
		self.training_args = training_args
		self.max_layer_count = discriminator_config.num_hidden_layers
		self.use_PS = training_args.use_PS
		self.PS_k = training_args.PS_k
		self.PS_mask = int(training_args.PS_mask, 2)
		self.discriminator_config = discriminator_config
		self.tokenizer = tokenizer
		self.discriminator_weight = training_args.discriminator_weight
		self.generator_weight = training_args.generator_weight

		self.mask_probability = data_args.mask_probability
		self.max_predictions_per_sequence = data_args.max_predictions_per_sequence
		# Original implementation has a default of :(mask_probability + 0.005) * max_sequence_length
		if self.max_predictions_per_sequence == -1:
			self.max_predictions_per_sequence = (self.mask_probability + 0.005) * data_args.block_size

		self.logging_step = training_args.logging_steps * training_args.gradient_accumulation_steps
		self.logging_i = 0

		self.generator = ElectraForMaskedLM(generator_config)

		if self.use_PS:
			self.discriminator = None
			self.stack_discriminator(0)
		else:
			self.create_discriminator()

		# Embeddings are shared
		#self.discriminator.set_input_embeddings(self.generator.get_input_embeddings())
		#self.discriminator.resize_token_embeddings(len(self.tokenizer))
		self.generator.resize_token_embeddings(len(tokenizer))

	def create_discriminator(self):
		self.discriminator = ElectraForPreTrainingWithTNF(self.discriminator_config, training_args=self.training_args, sequence_length=self.sequence_length)

		# Embeddings are shared
		self.discriminator.set_input_embeddings(self.generator.get_input_embeddings())
		self.discriminator.resize_token_embeddings(len(self.tokenizer))

	def stack_discriminator(self, stack_index):
		stack_size = self.max_layer_count // self.PS_k
		self.discriminator_config.num_hidden_layers = (stack_index + 1) * stack_size

		previous_model = self.discriminator
		self.create_discriminator()

		# Debug
		print(f"Discriminator layer count {self.discriminator_config.num_hidden_layers}")
		#for v in self.discriminator.state_dict():
		#	print(v)

		# Copy weights from previous model and stack them
		if previous_model:
			def check_mask(mask):
				return self.PS_mask & mask == mask

			# Stacking options
			stack_intermediate = check_mask(0b100)
			stack_output = check_mask(0b010)
			stack_attention_output = check_mask(0b001)

			mask_txt = f"Stack mask: {stack_intermediate} {stack_output} {stack_attention_output}"
			logger.info(mask_txt)
			print(mask_txt)

			stack_threshold = (stack_index - 1) * stack_size
			logger.info(f"size {stack_size},  threshold {stack_threshold}")

			new_state = self.discriminator.state_dict()
			count = 0
			previous_param_count = 0

			for layer in previous_model.state_dict():
				start = "electra.encoder.layer."
				if layer.startswith("electra.embeddings") or layer.startswith(start):
					previous_param_count += 1

				# Always copy previous layers in full
				new_state[layer] = previous_model.state_dict()[layer].clone()

				# Encoder layers can also be stacked
				if layer.startswith(start):
					mid = layer[len(start):]
					number = int(mid[:mid.find(".")])

					if number >= stack_threshold:
						end = mid[mid.find("."):]
						# Optionally some modules are reinitialized	
						if not stack_output and end.startswith(".output"):
							continue
						if not stack_intermediate and end.startswith(".intermediate"):
							continue
						if not stack_attention_output and end.startswith(".attention.output"):
							continue
						new_state[start+str(number+stack_size)+end] = previous_model.state_dict()[layer].clone()
						count += 1

			self.discriminator.load_state_dict(new_state)
			print(f"Stacked {count} layers")

			# Freeze previous layers (PS 2.0)
			#for i, t in enumerate(self.discriminator.named_parameters()):
			#	name = t[0]
			#	param = t[1]
			#	if not "tnf_notes" in name:
			#		param.requires_grad = i >= previous_param_count - 1
			#	logger.info(f"{name} grad:{param.requires_grad} {i}")
			#print(f"Froze {previous_param_count} layers")
		
	def fullgrad_discriminator(self):
		# In last extra phase full model is unfrozen (PS 2.0)
		for name, param in self.discriminator.named_parameters():
			if not "tnf_notes" in name:
				param.requires_grad = True
			#logger.info(f"{name} grad: {param.requires_grad}")
		
	def mask_inputs(self, input_ids: torch.Tensor, tokens_to_ignore, proposal_distribution=1.0):
		input_ids = input_ids.clone()
		inputs_which_can_be_masked = torch.ones_like(input_ids)
		for token in tokens_to_ignore:
			inputs_which_can_be_masked -= torch.eq(input_ids, token).long()

			total_number_of_tokens = input_ids.shape[-1]

		# Identify the number of tokens to be masked, which should be: 1 < num < max_predictions per seq.
		# It is set to be: n_tokens * mask_probability, but is truncated if it goes beyond bounds.
		number_of_tokens_to_be_masked = torch.min(
			torch.tensor(self.max_predictions_per_sequence, dtype=torch.long),
			torch.tensor(int(total_number_of_tokens * self.mask_probability), dtype=torch.long)
			).clamp(1)

		# The probability of each token being masked
		sample_prob = proposal_distribution * inputs_which_can_be_masked
		sample_prob /= torch.sum(sample_prob)

		# Sample from the probabilities
		masked_lm_positions = sample_prob.multinomial(number_of_tokens_to_be_masked)

		# Gather the IDs from the positions
		masked_lm_ids = input_ids.gather(-1, masked_lm_positions)

		# only masked values should be counted in the loss; build a tensor containing the true values and -100 otherwise
		masked_lm_labels = torch.full_like(input_ids, -100)
		masked_lm_labels.scatter_(-1, masked_lm_positions, masked_lm_ids)

		# Create a tensor filled with masks
		masked_tokens = torch.full_like(masked_lm_ids, self.tokenizer.mask_token_id)
		masked_lm_inputs = input_ids.clone()

		# Of the evaluated tokens, 15% of those will keep their original tokens
		replace_with_mask_positions = torch.rand_like(
						masked_lm_positions.float(),
						device=masked_lm_positions.device
						) < (1 - self.mask_probability)

		indices = masked_lm_positions.clone()
		indices[~replace_with_mask_positions] = 0

		# Add mask tokens at masked positions
		masked_lm_inputs.scatter_(-1, indices, masked_tokens)
		# Previous operation ruins class tokens, add them back
		masked_lm_inputs[..., 0] = 101

		return masked_lm_inputs, masked_lm_positions, masked_lm_labels

	@staticmethod
	def gather_positions(sequence, positions):
		batch_size, sequence_length, dimension = sequence.shape
		position_shift = (sequence_length * torch.arange(batch_size, device=sequence.device)).unsqueeze(-1)

		flat_positions = (positions + position_shift).long().view([-1])
		flat_sequence = sequence.view([batch_size * sequence_length, dimension])
		gathered = flat_sequence.index_select(0, flat_positions)
		return torch.reshape(gathered, [batch_size, -1, dimension])
	
	def forward(self, input_ids=None, attention_mask=None, token_type_ids=None, position_ids=None, head_mask=None, labels=None, tnf_masks=None):
		# get the masked positions as well as their original values
		masked_lm_inputs, masked_lm_positions, masked_lm_labels = self.mask_inputs(
			input_ids, [self.tokenizer.cls_token_id, self.tokenizer.sep_token_id, self.tokenizer.mask_token_id],
		)

		generator_loss, generator_output, generator_pred = self.generator(
						masked_lm_inputs,
						attention_mask,
						token_type_ids,
						position_ids,
						head_mask,
						position_ids,
						labels=masked_lm_labels,
						return_dict=False
						)

		# get the generator's predicted value on each masked position
		fake_logits = self.gather_positions(generator_output, masked_lm_positions)
		fake_argmaxes = fake_logits.argmax(-1)

		# create a tensor containing the predicted tokens
		fake_tokens = input_ids.scatter(-1, masked_lm_positions, fake_argmaxes)
		fake_tokens[:, 0] = input_ids[:, 0]
		discriminator_labels = (labels != fake_tokens).int()

		# TNF notes are updated based on generator output, not discriminator output, as suggested by the TNF paper
		# Notes are applied to discriminator embeddings inside discriminator model
		self.discriminator.electra.update_TNF_notes(generator_pred.detach(), tnf_masks)

		discriminator_loss, discriminator_output = self.discriminator(
						fake_tokens,
						attention_mask,
						token_type_ids,
						position_ids,
						head_mask,
						position_ids,
						labels=discriminator_labels,
						tnf_masks=tnf_masks
						)[:2]

		discriminator_predictions = torch.round((torch.sign(discriminator_output) + 1.0) * 0.5)

		total_loss = (self.discriminator_weight * discriminator_loss) + (self.generator_weight * generator_loss)
		
		# Debug info
		if (self.logging_i % self.logging_step == 0):
			print("generator_loss ", generator_loss)
			print("discriminator_loss ", discriminator_loss)

		self.logging_i += 1

		return (total_loss,)

	def save_pretrained(self, directory, state_dict):
		generator_path = os.path.join(directory, "generator")
		discriminator_path = os.path.join(directory, "discriminator")

		if not os.path.exists(generator_path):
			os.makedirs(generator_path)

		if not os.path.exists(discriminator_path):
			os.makedirs(discriminator_path)

		self.generator.save_pretrained(generator_path)
		self.discriminator.save_pretrained(discriminator_path)
		self.tokenizer.save_pretrained(discriminator_path)

# Support for constant scheduler
# Copied from trainer.py
def create_optimizer_and_scheduler(model, args, warmup_steps):
	no_decay = ["bias", "LayerNorm.weight"]
	optimizer_grouped_parameters = [
	{"params": [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)],
	"weight_decay": args.weight_decay},
	{"params": [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)],
	"weight_decay": 0.0},
	]

	optimizer = AdamW(
		optimizer_grouped_parameters,
		lr=args.learning_rate,
		betas=(args.adam_beta1, args.adam_beta2),
		eps=args.adam_epsilon,
	)

	if args.use_constant_scheduler == 1:
		lr_scheduler = get_constant_schedule_with_warmup(optimizer, warmup_steps)
	else:
		lr_scheduler = get_linear_schedule_with_warmup(
    		optimizer, num_warmup_steps=warmup_steps, num_training_steps=args.max_steps)

	return (optimizer, lr_scheduler)

def main():
	parser = HfArgumentParser((ModelArguments, DataTrainingArguments, ElectraTrainingArguments))
	model_args, data_args, training_args = parser.parse_args_into_dataclasses()

	if (
		os.path.exists(training_args.output_dir)
		and os.listdir(training_args.output_dir)
		and training_args.do_train
		and not training_args.overwrite_output_dir):
		raise ValueError(f"Output directory ({training_args.output_dir}) already exists and is not empty. Use --overwrite_output_dir to overcome.")

	# Setup logging
	logging.basicConfig(
		format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
		datefmt="%m/%d/%Y %H:%M:%S",
		level=logging.INFO if training_args.local_rank in [-1, 0] else logging.WARN)

	logger.warning(
		"Process rank: %s, device: %s, n_gpu: %s, distributed training: %s, 16-bits training: %s",
		training_args.local_rank,
		training_args.device,
		training_args.n_gpu,
		bool(training_args.local_rank != -1),
		training_args.fp16)

	logger.info(training_args)
	print(training_args)

	set_seed(training_args.seed)
	vocab_dir = os.path.join(model_args.tokenizer_name, "vocab.txt")

	# Load pretrained model and tokenizer

	if model_args.discriminator_config_name:
		discriminator_config = AutoConfig.from_pretrained(
			model_args.discriminator_config_name, cache_dir=model_args.cache_dir)
	elif model_args.discriminator_name_or_path:
		discriminator_config = AutoConfig.from_pretrained(
			model_args.discriminator_name_or_path, cache_dir=model_args.cache_dir)
	else:
		raise ValueError("Either --discriminator_config_name or --discriminator_name_or_path should be specified.")

	if model_args.generator_config_name:
		generator_config = AutoConfig.from_pretrained(model_args.generator_config_name, cache_dir=model_args.cache_dir)
	elif model_args.generator_name_or_path:
		generator_config = AutoConfig.from_pretrained(
			model_args.generator_name_or_path, cache_dir=model_args.cache_dir)
	else:
		raise ValueError("Either --generator_config_name or --generator_name_or_path should be specified.")

	tokenizer = ElectraTokenizer(vocab_dir)

	logger.info(discriminator_config)
	logger.info(generator_config)
	print(discriminator_config)
	print(generator_config)

	if training_args.use_TNF:
		__, rare_words = create_rare_word_tables(training_args.TNF_word_list_path)
		training_args.TNF_word_count = len(rare_words)

	model = CombinedModel(discriminator_config, generator_config, tokenizer, training_args, data_args, model_args)

	train_dataset = get_dataset(data_args, training_args)

	start_time = time.time()
	def printCurrentTime():
		delta = datetime.timedelta(seconds = time.time() - start_time)
		txt = "Total duration: " + str(delta)
		print(txt)
		logger.info(txt)

	# Masking is done inside the CombinedModel
	# This collator simply batches samples together
	data_collator = ElectraCollator(tokenizer=tokenizer, use_TNF=training_args.use_TNF)

	use_PS = training_args.use_PS

	if training_args.do_train:
		max_steps = training_args.max_steps
		save_steps = training_args.save_steps
		# PS 2.0 should have k + 1 for the last extra phase
		k = training_args.PS_k if use_PS else 1
		learning_rate = training_args.learning_rate

		warmup_steps = training_args.warmup_steps
		print(f"steps {max_steps} warmup {warmup_steps} lr {learning_rate}")

		for i in range(k):
			last_phase = i == k - 1
			steps = max_steps
		
			if use_PS:
				steps = max_steps // k
				# Checkpoint saving disabled while stacking
				training_args.save_steps = 999999999999
				if i == 0:
					pass
				else:
					warmup_steps = 0
					# In last extra phase full model is unfrozen (PS 2.0)
					#if last_phase:
					#	logger.info("Retraining full model")
					#	training_args.save_steps = save_steps
					#	model.fullgrad_discriminator()
					#else:
					#	model.stack_discriminator(i)
					model.stack_discriminator(i)

				text = f"Progressive stacking {i+1}: steps {steps} warmup {warmup_steps} learning_rate {learning_rate}"
				print(text)
				logger.info(text)

			training_args.max_steps = int(steps)

			# Linear scheduler only works without PS
			optimizer, lr_scheduler = create_optimizer_and_scheduler(model, training_args, warmup_steps)
			logger.info(optimizer)
			print(optimizer)

			# Training
			trainer = Trainer(
				model=model,
				args=training_args,
				data_collator=data_collator,
				train_dataset=train_dataset,
				optimizers=	(optimizer, lr_scheduler)
			)

			trainer.train()
			learning_rate = lr_scheduler.get_last_lr()[0] 
			printCurrentTime()

			if last_phase:
				model_path = training_args.output_dir
				model.discriminator.save_pretrained(model_path)
				tokenizer.save_pretrained(model_path)

if __name__ == "__main__":
	main()
