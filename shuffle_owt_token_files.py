import sys, os, random, array

folder_path = sys.argv[1]
shuffle_suffix = sys.argv[2]
tokens_per_line = int(sys.argv[3])
lines_per_file = int(sys.argv[4])

#Debug
#with open(os.path.join(folder_path, "0"), "rb") as f:
#	a = array.array("i")
#	for i in range(4):
#		a.fromfile(f, tokens_per_line)
#		print(i, "\n", a)

shuffle_path = folder_path + shuffle_suffix
if not os.path.exists(shuffle_path):
	os.mkdir(shuffle_path)

file_names = sorted(os.listdir(folder_path), key=lambda x:int(x))
# Last file is probably not complete. It is skipped for simplicity
file_names = file_names[:-1]
file_count = len(file_names)

def printProgress(i, l):
	print(f"{int(i/float(l)*100)}%", end="\r")

print("Create and shuffle line index list")
total_lines = file_count * lines_per_file
random_lines = [0] * total_lines
for i in range(total_lines):
	random_lines[i] = i
	printProgress(i, total_lines)

random.shuffle(random_lines)

print("Read all lines into RAM")
token_lines = []
for n in range(file_count):
	name = file_names[n]
	with open(os.path.join(folder_path, name), "rb") as f:
		for i in range(lines_per_file):
			line = array.array("i")
			line.fromfile(f, tokens_per_line)
			token_lines.append(line)
	printProgress(n, file_count)

print("Write lines to files")
global_index = 0
file_index = 0

while file_index < file_count:
	# Open output file for writing
	with open(os.path.join(shuffle_path, file_names[file_index]), "wb") as f:
		file_index += 1

		# Write lines to file
		for i in range(lines_per_file):
			f.write(token_lines[random_lines[global_index]])
			global_index += 1

	printProgress(file_index, file_count)

print("Done!")

#Debug
#with open(os.path.join(shuffle_path, "0"), "rb") as f:
#	a = array.array("i")
#	for i in range(4):
#		a.fromfile(f, tokens_per_line)
#		print(i, "\n", a)
