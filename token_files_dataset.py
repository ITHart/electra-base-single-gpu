import os, array

import torch
from torch.utils.data import IterableDataset

class TokenFilesDataset(IterableDataset):
	def __init__(self, token_files_path, TNF_mask_files_path = None, block_size=512):
		self.use_masks = TNF_mask_files_path != None

		assert os.path.isdir(token_files_path)
		if self.use_masks:
			self.masks_path = TNF_mask_files_path
			assert os.path.isdir(self.masks_path)
		
		self.block_size = block_size
		self.tokens_path = token_files_path 
		self.f = None

		# Length calculation assumes the directory only has valid data files named from 0 to [file_count - 1]
		# TNF mask files are supposed to be identical in form
		file_count = len(os.listdir(self.tokens_path))
		self.file_index = 0
		self.file_index_max = file_count - 1
		
		# Calculate dataset size. The last file might have less lines, so it is read separately.
		with open(os.path.join(self.tokens_path, str(self.file_index)), "rb") as f:
			tokens = array.array("i", f.read())
			self.length = (len(tokens) // self.block_size) * (file_count - 1)

		with open(os.path.join(self.tokens_path, str(self.file_index_max)), "rb") as f:
			tokens = array.array("i", f.read())
			self.length += len(tokens) // self.block_size

		print(f"Dataset {self.tokens_path}\n{TNF_mask_files_path}\nlength: {self.length}, file count: {file_count}")

	def __len__(self):
		return self.length

	def iterate_files(self):
		# Repetitive code alert!
		if self.use_masks:
			# Read token and mask files in tandem
			while(True):
				# Open next file or stop iteration
				if self.f == None:
					if self.file_index > self.file_index_max:
						break
					tokens_path = os.path.join(self.tokens_path, str(self.file_index))
					masks_path = os.path.join(self.masks_path, str(self.file_index))
					if not os.path.exists(tokens_path) or not os.path.exists(masks_path):
						break
					self.f = open(tokens_path, "rb")
					self.masks_f = open(masks_path, "rb")
					
					self.file_index += 1

				# Token files and mask files have to be the exact same size for this contraption to work
				try:
					tokens_line = array.array("i")
					tokens_line.fromfile(self.f, self.block_size)
				except:
					self.f.close()
					self.f = None
					self.masks_f.close()
					self.masks_f = None
					continue
				
				masks_line = array.array("i")
				masks_line.fromfile(self.masks_f, self.block_size)

				yield [torch.tensor(tokens_line, dtype=torch.long), torch.tensor(masks_line, dtype=torch.long)]
		else:
			# Only read token files
			while(True):
				# Open next file or stop iteration
				if self.f == None:
					if self.file_index > self.file_index_max:
						break	
					path = os.path.join(self.tokens_path, str(self.file_index))
					if not os.path.exists(path):
						break
					self.f = open(path, "rb")
					self.file_index += 1

				try:
					tokens_line = array.array("i")
					tokens_line.fromfile(self.f, self.block_size)
				except:
					# End of file
					self.f.close()
					self.f = None
					continue

				yield torch.tensor(tokens_line, dtype=torch.long)

	def __iter__(self):
		return iter(self.iterate_files())
